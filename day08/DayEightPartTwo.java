package day08;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import helpers.*;

/**
 * Day 8 Part 2 of Advent of Code:
 * Continuing from part 1, parse input data to get readable output. Then, 
 * sum all values.
 *
 * @author Blizzard Finnegan
 */
public class DayEightPartTwo
{
  public static void main(String[] args)
  {
    final int OUTPUT_OFFSET = 10;
    //Import file
    List<String> fileImport = FileRead.readFile("day08/data/input.txt");

		//Arraylist of all signals and their outputs
    ArrayList<ArrayList<String>> signalAndOutputList = new ArrayList<>();

		//Iterate through each line of the file...
    for(String line : fileImport)
    {
			//Create an ArrayList for storage
      ArrayList<String> signalAndOutput = new ArrayList<>();

			//split the line, iterate over it, and store it
      String[] splitLine = line.split(" ");
      for(int i = 0; i < splitLine.length; i++)
      {
        if(splitLine[i] != "|") 
        {
          signalAndOutput.add(splitLine[i]);
        }
      }

			//Add the parsed line into the arraylist
      signalAndOutputList.add(signalAndOutput);
    }

		//Sum variable used later
    int finalSum = 0;

		//Iterate over the arraylist
    for(ArrayList<String> line : signalAndOutputList)
    {
			//HashMap to convert each input/output into an integer
			//Key is string; refers to the input
			//Value is int; refers to parsed 7-segment display
			HashMap<String,Integer> conversionValueMap = new HashMap<>();
			HashMap<Integer,String> conversionKeyMap = new HashMap<>();
			ArrayList<String> inputOptions = new ArrayList<>();

			//default to not doing anything (don't parse the input files)
      boolean inputSide = true;

			//Bit location counter, so we don't have to rework the for-each loop
			int bitCounter = 3;

			//Iterate through each string in the line
      for(String value : line)
      {
				//If the current string is a pipe, start parsing the input options
        if(value.equals("|")) 
				{

					inputSide = false;
					/* for the following 3 loops, the following logic is being used
					 * Output | Length | Check   | Loop #
					 *    1   |   2    | Length    	 | 1
					 *    7   |   3    | Length    	 | 1
					 *    4   |   4    | Length    	 | 1
					 *    8   |   7    | Length    	 | 1
					 *    0   |   6    | contains 4  | 2
					 *    3   |   5    | contains 1	 | 2
					 *    6   |   6    | contains 1	 | 2
					 *    9   |   6    | contains 4	 | 2
					 *    5   |   5    | 6 contains  | 3
					 *    2   |   5    | elimination | 3
					 */
					//Iterate through all inputs; trim out the easy options; add to map
					for(String nextInput : inputOptions)
					{
						//Check the length of the string
						//Convert the string into the integer according to the aforementioned chart
						switch(nextInput.length())
						{
							case 2:
								conversionValueMap.put(nextInput,1);
								conversionKeyMap.put(1,nextInput);
								break;

							case 3:
								conversionValueMap.put(nextInput,7);
								conversionKeyMap.put(7,nextInput);
								break;
							
							case 4:
								conversionValueMap.put(nextInput,4);
								conversionKeyMap.put(4,nextInput);
								break;

							case 7:
								conversionValueMap.put(nextInput,8);
								conversionKeyMap.put(8,nextInput);
								break;

							//If the length isn't enough to figure out what it is, wait
							default:
								break;
						}
					}

					//Remove values already in the map
					inputOptions.removeAll(conversionValueMap.keySet());

					//Iterate through list again; checking for the harder tests
					for(String input : inputOptions)
					{
						switch(input.length())
						{
							//This should only check for 3. 5 and 2 need another loop.
							//If the input string is of length 5 ...
							case 5:
								//... and contains all values within 1
								if(singleCompareStringContents(conversionKeyMap.get(1),input))
								{
									//Add it to the maps as 3
									conversionValueMap.put(input,3);
									conversionKeyMap.put(3,input);
									break;
								}

								//... and it doesn't contain all values within 1
								else
								{
									//Save for another loop
									break;
								}

							//If the input string is of length 6 ...
							case 6:

								//... and it contains all the values in 4
								if(singleCompareStringContents(conversionKeyMap.get(4),input))
								{
									//Add it to the maps as 9
									conversionValueMap.put(input,9);
									conversionKeyMap.put(9,input);
									break;
								}

								//... and it contains all the values in 1
								else if(singleCompareStringContents(conversionKeyMap.get(1),input))
								{
									//Add it to the maps as 0
									conversionValueMap.put(input,0);
									conversionKeyMap.put(0,input);
									break;
								}
								//... and it doesn't contain either 4 or 1
								else
								{
									//add it to the maps as 6
									conversionValueMap.put(input,6);
									conversionKeyMap.put(6,input);
									break;
								}

							default:
								System.out.println("Misses an input!");
								break;
						}
					}

					//Remove values already in the map
					inputOptions.removeAll(conversionValueMap.keySet());

					//All remaining are either a 5 or a 2
					for(String input : inputOptions)
					{
						if(singleCompareStringContents(input,conversionKeyMap.get(6)))
						{
							conversionValueMap.put(input,5);
							conversionKeyMap.put(5,input);
						}
						else
						{
							conversionValueMap.put(input,2);
							conversionKeyMap.put(2,input);
						}
					}
				}

        else if(inputSide) 
				{
					//Add input option into a temporary storage place
					inputOptions.add(value);
					continue;
				}

				//If we're on the output side...
        else
        {
					//TODO: Use the maps created on the input side to create the output number
					//			then add it to the total
					for(String key : conversionValueMap.keySet())
					{
						if(compareStringContents(value,key))
						{
							//Add the current value to the total sum 
							finalSum += conversionValueMap.get(key) * Math.pow(10,bitCounter);

							//Increment the place index.
							bitCounter--;
							//System.out.print(conversionValueMap.get(key));

							//Proceed to the next value in the line
							break;
						}
					}
        }
      }
    }

		//Print the final output
    System.out.println("Total Sum: " + finalSum);
  }

	/**
	 * Internal method that checks that strings contain the same characters,
	 * regardless of order. Check is run both ways, so order of strings is
	 * irrelevant.
	 *
	 * @param s1 - The first string to compare
	 * @param s2 - The second string to compare
	 *
	 * @return boolean - Whether or not the strings are equal in content
	 */
	private static boolean compareStringContents(String s1, String s2)
	{
		return singleCompareStringContents(s1,s2) && singleCompareStringContents(s2,s1);
	}
	
	/**
	 * Internal method that checks that one string contains another entirely, 
	 * regardless of order. Check is one-sided, so order is important
	 * irrelevant.
	 *
	 * @param s1 - The first string to compare. All characters will be checked.
	 * @param s2 - The second string to compare
	 *
	 * @return boolean - Whether or not the strings are equal in content
	 */
	private static boolean singleCompareStringContents(String s1, String s2)
	{
		for(Character value : s1.toCharArray() )
		{
			if(!s2.contains(value.toString())) return false;
		}
		return true;
	}
}
