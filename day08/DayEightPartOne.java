package day08;

import java.util.ArrayList;
import java.util.List;

import helpers.*;
/**
 * Day 8 Part 1 of Advent of Code:
 * Input file contains 10 space-separated-strings, followed by a '|', then 
 * 4 space-separated-strings. Each letter in the String represents a light on a 
 * seven-segment display. Letter-to-light binding is consistent in each line, but 
 * not across the file. Using only the output, determine how many strings are either 
 * a 1, 4, 7, or 8. (This can be easily done by checking the length of each string.)
 * 
 * @author Blizzard Finnegan
 */
public class DayEightPartOne
{
  public static void main(String[] args)
  {
    final int OUTPUT_OFFSET = 10;
    //Import file
    List<String> fileImport = FileRead.readFile("day08/data/input.txt");

		//Arraylist of all signals and their outputs
    ArrayList<ArrayList<String>> signalAndOutputList = new ArrayList<>();

		//Iterate through each line of the file...
    for(String line : fileImport)
    {
			//Create an ArrayList for storage
      ArrayList<String> signalAndOutput = new ArrayList<>();

			//split the line, iterate over it, and store it
      String[] splitLine = line.split(" ");
      for(int i = 0; i < splitLine.length; i++)
      {
        if(splitLine[i] != "|") 
        {
          signalAndOutput.add(splitLine[i]);
        }
      }

			//Add the parsed line into the arraylist
      signalAndOutputList.add(signalAndOutput);
    }

		//Count the number of recognizable characters
    int recognizableCount = 0;

    //Simplified for part 1
		//Iterate over the arraylist
    for(ArrayList<String> line : signalAndOutputList)
    {
			//default to not doing anything (don't parse the input files)
      boolean inputSide = true;

			//Iterate through each string in the line
      for(String value : line)
      {
				//If the current string is a pipe, start parsing
        if(value.equals("|")) 
				{
					inputSide = false;
				}

				//If we're still on the input side, don't do anything
        else if(inputSide) 
				{
					continue;
				}

				//If we're on the output side...
        else
        {
					//Debug

					//If the string has a recognizable length, increment the counter
          switch (value.length()) {
            case 2:
            case 3:
            case 4:
            case 7:
              recognizableCount++;
              break;
          
					//Otherwise, do nothing
            default:
              break;
          }
        }
      }
    }

		//Print the final output
    System.out.println("Number of recognizable digits: " + recognizableCount);
  }
}
