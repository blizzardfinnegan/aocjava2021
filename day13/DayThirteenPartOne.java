package day13;

import helpers.*;

/**
 * Day 13 Part 1 of Advent of Code 2021:
 * 
 * Import file contains coordinates, followed by a blank space, then fold instructions
 *
 * Formatting of coordinates:
 * 6,10
 *
 * Formatting of folding instructions:
 * fold along y=7
 *
 * Mark all points on a paper. Fold according to instructions. (If y=, fold up; if x=, fold left)
 *
 * Output the number of visible points after all folds
 *
 * Test input should output either 16 or 17
 *
 * @author Blizzard Finnegan
 */
public class DayThirteenPartOne
{
	public static void main(String[] args)
	{
		/*
		 *
		 * create arraylist of arraylist of booleans for coordinates
		 * create arraylist of intpairs for hole locations
		 * create arraylist of intpairs for fold instructions
		 *
		 * import file
		 *
		 * for each line in file
		 *		if coordinate, add to arraylist (store largest x and y value)
		 *		if fold instructions, add to fold instructions arraylist
		 *        for folds y=, use -1 for the x value
		 *				for folds x=, use -1 for the y value
		 *
		 * fill boolean nested arraylist with false
		 *
		 * iterate through hole location
		 *		mark each coordinate in the nested arraylist as true
		 *
		 * iterate through fold instructions
		 *		make temp new coordinate list, of size half - 1 in the folding direction
		 *    OR together the boolean on either side of the line (if there isn't two sides, just pass it over)
		 *		overwrite outside coordinate nested arraylist
		 *
		 * iterate over coordinate list
		 *    count the number of TRUEs
		 *
		 * print the number of trues
		 */
	}
}
