package day14;

import helpers.*;

/**
 * Day 14 Part 1 of Advent of Code 2021:
 *
 * Imported file contains an initial line, followed by a insertion scheme, separated by an empty line.
 *
 * An initial line is shown below as an example:
 * NNCB
 * This will be referred to as "the string".
 *
 * A insertion scheme is shown below as an example:
 * CH -> B
 *
 * For each insertion scheme, the following logic is applied:
 * If the 2 character pair exists in the string, add the end character between the two.
 * All rules are applied SIMULTANEOUSLY. 
 *
 * Apply all rules 10 times in a row. Then, count each kind of letter.
 * Subtract the highest count from the lowest count, then output this number.
 *
 * The test input should output 1588
 * 
 * @author Blizzard Finnegan
 */
public class DayFourteenPartOne
{
	public static void main(String[] args)
	{
		/*
		 * arraylist of characters
		 * hashmap1; keys of string, value of character
		 * hashmap2; keys of character, value of integer
		 *
		 * import file
		 *
		 * read the first line in the file into the arraylist of characters
		 *
		 * remove the first to lines of the imported file
		 *
		 * for line in in file
		 *		split on " -> "
		 *		add split line to the hashmap
		 *		if the single character isn't in the keyset, add it to map2 (all values set here are 0)
		 *
		 * loop 10 times
		 *		create temp arraylist of characters
		 *		for(character in current arraylist) [do this as a traditional for loop]
		 *				add the current character to the temp arraylist
		 *				if the current character and the next character are a key in the keymap [containsKey]
		 *						add the associated value to the temp arraylist
		 *		overwrite the current arraylist with the temp
		 *
		 * iterate over current arraylist
		 *		using the current character as the key, increment the value in hashmap2
		 *
		 * iterate over hashmap2
		 *		get smallest and largest values
		 *
		 * subtract smallest from largest
		 *
		 * print final value
		 */
	}
}
