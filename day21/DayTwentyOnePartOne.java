package day21;

import helpers.*;

/**
 * Day 21 Part 1 of Advent of Code 2021:
 *	Imported file contains the following two lines:
 *
 *	Player 1 starting position: 4
 *	Player 2 starting position: 5
 *
 *	Test input changes player 2 starting position to 8. 
 *
 * From their starting position, each player moves forward a predefined amount 
 * of spaces around a 10-space ring. Their final position is then added to their 
 * score. 
 *
 * Players move forward 1 more space than the previous move. So, Player 1 will move
 * forward 1 space, then 2 spaces, then 3 spaces. Afterwards, Player 2 will move 
 * forward 4 spaces, then 5 spaces, then 6 spaces, and so on. When this number hits 100, 
 * it rolls back over to 1.
 *
 * The game continues until one player has at least 1000 points. The final score of 
 * the game is calculated as the losing player's score multiplied by the number of 
 * moves made.
 *
 * The test input retuns a value of 739785.
 *
 * @author Blizzard Finnegan
 */
public class DayTwentyOnePartOne
{
	public static void main(String[] args)
	{
	}
}
