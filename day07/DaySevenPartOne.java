package day07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import helpers.*;

/**
 * AdventOfCode Day7: Move all the crab submarines to the same position, with the least amount of fuel spent
 * 
 * @author Blizzard Finnegan
 */
public class DaySevenPartOne
{
  public static void main(String[] args) 
  {
    //Import file; parse into arraylist of crabs
    List<String> fileImport = FileRead.readFile("day07/data/input.txt");
    ArrayList<Integer> crabs = new ArrayList<>();
    String fileLine = fileImport.remove(0);
    String[] splitLine = fileLine.split(",");
    for(String crabString : splitLine)
    {
      crabs.add(Integer.parseInt(crabString));
    }

    //The maximum distance a crab can move
    int maxDisplacement = Collections.max(crabs);

    //The lowest fuel cost; starting with "all of it"
    int bestFuelCost = Integer.MAX_VALUE;

    //Iterate through all possible locations
    for(int testFinalPosition = 0; testFinalPosition <= maxDisplacement; testFinalPosition++)
    {
      //Create a fuel cost, and see how much fuel it would take to move all crabs to that position
      int fuelCost = 0;
      for(Integer crab : crabs)
      {
        fuelCost += Math.abs(crab - testFinalPosition);
      }

      //If its a lower fuel cost than the current best, overwrite current best
      if(fuelCost < bestFuelCost)
      {
        bestFuelCost = fuelCost;
      }
    }

    //Print final answer
    System.out.println("Best fuel cost: " + bestFuelCost);
  }  
}
