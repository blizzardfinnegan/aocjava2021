package day10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.stream.Collectors;

import helpers.*;

/**
 * Day Ten Part One  of Advent of Code:
 * Imported file is a series of nested braces. All possible brace sets are listed below:
 * ()
 * <>
 * {}
 * []
 *
 * Some lines have mis-matched brace pairs. Ignore these lines.
 *
 * The test solution should have a total point count of 26397
 *
 * @author Blizzard Finnegan
 */
public class DayTenPartTwo
{
	public static void main(String[] args)
	{
		//Sets of available characters and their types
		//I wanted to just use enums, but it got unhappy with that
		HashSet<Character> openSet = new HashSet<>();
		openSet.add('[');
		openSet.add('(');
		openSet.add('<');
		openSet.add('{');

		HashSet<Character> closeSet = new HashSet<>();
		closeSet.add('}');
		closeSet.add('>');
		closeSet.add(')');
		closeSet.add(']');

		HashMap<Character,Character> openCloseMap = new HashMap<>();
		openCloseMap.put('[',']');
		openCloseMap.put('(',')');
		openCloseMap.put('{','}');
		openCloseMap.put('<','>');

		HashMap<Character,Character> closeOpenMap = new HashMap<>();
		closeOpenMap.put(']','[');
		closeOpenMap.put(')','(');
		closeOpenMap.put('}','{');
		closeOpenMap.put('>','<');

		HashMap<Character,Integer> closePointMap = new HashMap<>();
		closePointMap.put(')',1);
		closePointMap.put(']',2);
		closePointMap.put('}',3);
		closePointMap.put('>',4);

		//Import file
		List<String> fileImport = FileRead.readFile("day10/data/testInput.txt");

		ArrayList<List<Character>> parsedFile = new ArrayList<>();

		for(String line : fileImport)
		{
			//Takes a string, converts it to a stream of chars. Stream is then fed into a collector, and saved as a list
			List<Character> parsedLine = line.chars().mapToObj(c -> (char)c).collect(Collectors.toList());

			parsedFile.add(parsedLine);
		}
		/*
		 * For each line in the parsed file
		 *		Create a stack to use
		 *		iterate through each character
		 *				if the character is an opening brace, add it to the stack
		 *				if the character is a closing brace
		 *						If its the matching half of the brace on top of the stack
		 *								remove the top of the stack, move on
		 *						If its a mis-match
		 *								Add it to a list of corrupted lines
		 * remove corrupted lines from parsed file
		 * 
		 */

		ArrayList<List<Character>> corruptedLines = new ArrayList<>();
		PriorityQueue<Integer> lineScores = new PriorityQueue<>();

		for(List<Character> line : parsedFile)
		{
			Stack<Character> workingStack = new Stack<>();
			int pointTotal = 0;
			boolean corrupted = false;
			System.out.println(line);
			for(Character point : line)
			{
				System.out.print(point + "\t");
				if(openSet.contains(point)) 
				{
				System.out.print(point + "\t");
					workingStack.push(point);
				}
				else if(closeSet.contains(point))
				{
					if(workingStack.peek().equals(closeOpenMap.get(point)))
					{
						workingStack.pop();
						continue;
					}
					else
					{
						corrupted = true;
						break;
					}
				}
				else continue;

				if(corrupted)
				{
					corruptedLines.add(line);
				}
				else
				{
					while(workingStack.size() > 0)
					{
						Character removedOpen = workingStack.pop();
						pointTotal *= 5;
						pointTotal += closePointMap.get(openCloseMap.get(removedOpen));
					}
				}
				System.out.println(workingStack);
			}
			lineScores.add(pointTotal);
		}

		int maxSize = lineScores.size();
		while(lineScores.size() > (maxSize / 2))
		{
			lineScores.poll();
		}
		int pointTotal = lineScores.poll();

		System.out.println("Error score: " + pointTotal);
	}
}
