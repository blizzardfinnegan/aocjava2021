package day10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

import helpers.*;

/**
 * Day Ten Part One  of Advent of Code:
 * Imported file is a series of nested braces. All possible brace sets are listed below:
 * ()
 * <>
 * {}
 * []
 *
 *These are nested, but not entirely correctly. Find the first mis-matched brace
 *		on each line, and according to the following table, find the total error score.
 *
 *	) : 3 points
 *  ] : 57 points
 *  } : 1197 points
 *  > : 25137 points
 *
 * The test solution should have a total point count of 26397
 *
 * @author Blizzard Finnegan
 */
public class DayTenPartOne
{
	public static void main(String[] args)
	{
		//Sets of available characters and their types
		//I wanted to just use enums, but it got unhappy with that
		HashSet<Character> openSet = new HashSet<>();
		openSet.add('[');
		openSet.add('(');
		openSet.add('<');
		openSet.add('{');

		HashSet<Character> closeSet = new HashSet<>();
		closeSet.add('}');
		closeSet.add('>');
		closeSet.add(')');
		closeSet.add(']');

		HashMap<Character,Character> closeOpenMap = new HashMap<>();
		closeOpenMap.put(']','[');
		closeOpenMap.put(')','(');
		closeOpenMap.put('}','{');
		closeOpenMap.put('>','<');

		HashMap<Character,Integer> closePointMap = new HashMap<>();
		closePointMap.put(')',3);
		closePointMap.put(']',57);
		closePointMap.put('}',1197);
		closePointMap.put('>',25137);

		//Import file
		List<String> fileImport = FileRead.readFile("day10/data/input.txt");

		ArrayList<List<Character>> parsedFile = new ArrayList<>();

		for(String line : fileImport)
		{
			//Takes a string, converts it to a stream of chars. Stream is then fed into a collector, and saved as a list
			List<Character> parsedLine = line.chars().mapToObj(c -> (char)c).collect(Collectors.toList());

			parsedFile.add(parsedLine);
		}
		/*
		 * For each line in the parsed file
		 *		Create a stack to use
		 *		iterate through each character
		 *				if the character is an opening brace, add it to the stack
		 *				if the character is a closing brace
		 *						If its the matching half of the brace on top of the stack
		 *								remove the top of the stack, move on
		 *						If its a mis-match
		 *								add to the point total, based on the BadBrace
		 */
		int pointTotal = 0;
		for(List<Character> line : parsedFile)
		{
			Stack<Character> workingStack = new Stack<>();
			for(Character point : line)
			{
				if(openSet.contains(point)) workingStack.add(point);
				else if(closeSet.contains(point))
				{
					if(workingStack.pop().equals(closeOpenMap.get(point)))
					{
						continue;
					}
					else
					{
						pointTotal += closePointMap.get(point);
					}
				}
			}
		}

		System.out.println("Error score: " + pointTotal);
	}
}
