package day09;

import java.util.ArrayList;
import java.util.List;

import helpers.*;

/**
 * Day 9 Part 1 of Advent of Code 2021:
 * Imported file is a map of points, with a given value (representing depth). 
 * As every character is a point, there are no separators. Low points are defined 
 * as having higher values to the left, right, up, and down.
 *
 * Find each of the lowest points. Add 1 to each of their values, and print the sum.
 *
 * testInput should return a final value of 15.
 *
 * @author Blizzard Finnegan
 */
public class DayNinePartOne
{
	public static void main(String[] args)
	{
		//Import file
		List<String> fileImport = FileRead.readFile("day09/data/input.txt");

		//Storage of all points on the imported map
		ArrayList<ArrayList<WeightedPoint>> mapOfPoints = new ArrayList<>();

		//Storage for lowest points
		ArrayList<WeightedPoint> lowPointList = new ArrayList<>();

		//Create counter for the y-axis
		int yCounter = 0;

		//Iterate through the file...
		for(String line : fileImport)
		{
			//Create a counter for the x-axis that resets on each y-line
			int xCounter = 0;

			//Working space for the current row
			ArrayList<WeightedPoint> translatedLine = new ArrayList<>();

			//Split the line into individual characters
			char[] splitLine = line.toCharArray();

			//For every point in the line
			for(char point : splitLine)
			{
				//Add the new point (of location xCounter,yCounter, weight point) to the working space
				translatedLine.add(new WeightedPoint(new IntPair(xCounter,yCounter),Character.getNumericValue(point)));

				//Increment xCounter
				xCounter++;
			}
			//Add line to the map
			mapOfPoints.add(translatedLine);

			//Increment yCounter
			yCounter++;
		}

		//Iterate through the map of points
		for(int row = 0; row < mapOfPoints.size(); row++)
		{
			for(int col = 0; col < mapOfPoints.get(0).size(); col++)
			{
				WeightedPoint currentLocation = mapOfPoints.get(row).get(col);

				//Break testing into sub-functions for each direction
				boolean leftCheck = lowestPointLeft(currentLocation,mapOfPoints);
				boolean rightCheck = lowestPointRight(currentLocation,mapOfPoints);
				boolean upCheck = lowestPointUp(currentLocation,mapOfPoints);
				boolean downCheck = lowestPointDown(currentLocation,mapOfPoints);

				//If all tests return true, then it is a lowest point
				if(leftCheck && rightCheck && upCheck && downCheck)
				{
					//Debug
					//System.out.println("new point: " + currentLocation.toString());

					//Add the lowest point to the list
					lowPointList.add(currentLocation);
				}
			}
		}

		//Calculate the final total by adding all of the 1-indexed weights of the lowest points
		int finalTotal = 0;
		for(WeightedPoint point : lowPointList)
		{
			finalTotal += point.getWeight() + 1;
		}

		//Output final total
		System.out.println("Final total: " + finalTotal);
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point to the right is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point to the right
	 */
	private static boolean lowestPointRight(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the x coordinate is already the right-most value,
		// there is no location to check. Return true to save error-checking
		if(xCoord + 1 >= mapOfPoints.get(yCoord).size()) return true;

		//If the weight of the test point is lower than the point to the right
		return testPoint.getWeight() < mapOfPoints.get(yCoord).get(xCoord + 1).getWeight();
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point to the left is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point to the left
	 */
	private static boolean lowestPointLeft(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the x coordinate is already the left-most value,
		// there is no location to check. Return true to save error-checking
		if(xCoord - 1 < 0) return true;

		//If the weight of the test point is lower than the point to the left
		return testPoint.getWeight() < mapOfPoints.get(yCoord).get(xCoord - 1).getWeight();
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point above is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point above
	 */
	private static boolean lowestPointUp(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the y coordinate is already the top-most value,
		// there is no location to check. Return true to save error-checking
		if(yCoord - 1 < 0) return true;

		//If the weight of the test point is lower than the point above
		return testPoint.getWeight() < mapOfPoints.get(yCoord - 1).get(xCoord).getWeight();
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point below is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point below
	 */
	private static boolean lowestPointDown(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the y coordinate is already the bottom-most value,
		// there is no location to check. Return true to save error-checking
		if(yCoord + 1 >= mapOfPoints.size()) return true;

		//If the weight of the test point is lower than the point below
		return testPoint.getWeight() < mapOfPoints.get(yCoord + 1).get(xCoord).getWeight();
	}
}
