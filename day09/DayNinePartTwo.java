package day09;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.PriorityQueue;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.Set;

import helpers.*;
import helpers.WeightedPoint;

/**
 * Day 9 Part 2 of Advent of Code 2021:
 * Imported file is a map of points, with a given value (representing depth). 
 * As every character is a point, there are no separators. Low points are defined 
 * as having higher values to the left, right, up, and down.
 *
 * Find the three largest basins. Take the size (found by adding all values in 
 * the basin except the 9 edges) of all three, and multiply them together.
 *
 * testInput should return a final value of 1134.
 *
 * @author Blizzard Finnegan
 */
public class DayNinePartTwo
{
	//Constants
	private static int X_MAX = 0;
	private static int Y_MAX = 0;

	public static void main(String[] args)
	{
		//Import file
		List<String> fileImport = FileRead.readFile("day09/data/input.txt");

		//Storage of all points on the imported map
		ArrayList<ArrayList<WeightedPoint>> mapOfPoints = new ArrayList<>();

		//Storage for lowest points
		ArrayList<WeightedPoint> lowPointList = new ArrayList<>();

		//Create counter for the y-axis
		int yCounter = 0;

		//Iterate through the file...
		for(String line : fileImport)
		{
			//Create a counter for the x-axis that resets on each y-line
			int xCounter = 0;

			//Working space for the current row
			ArrayList<WeightedPoint> translatedLine = new ArrayList<>();

			//Split the line into individual characters
			char[] splitLine = line.toCharArray();

			//For every point in the line
			for(char point : splitLine)
			{
				//Add the new point (of location xCounter,yCounter, weight point) to the working space
				translatedLine.add(new WeightedPoint(new IntPair(xCounter,yCounter),Character.getNumericValue(point)));

				//Increment xCounter
				xCounter++;
			}
			//Add line to the map
			mapOfPoints.add(translatedLine);

			//Increment yCounter
			yCounter++;
		}

		//Set constants
		X_MAX = mapOfPoints.get(0).size();
		Y_MAX = mapOfPoints.size();

		//Iterate through the map of points
		for(int row = 0; row < Y_MAX; row++)
		{
			for(int col = 0; col < X_MAX; col++)
			{
				WeightedPoint currentLocation = mapOfPoints.get(row).get(col);

				//Break testing into sub-functions for each direction
				boolean leftCheck = lowestPointLeft(currentLocation,mapOfPoints);
				boolean rightCheck = lowestPointRight(currentLocation,mapOfPoints);
				boolean upCheck = lowestPointUp(currentLocation,mapOfPoints);
				boolean downCheck = lowestPointDown(currentLocation,mapOfPoints);

				//If all tests return true, then it is a lowest point
				if(leftCheck && rightCheck && upCheck && downCheck)
				{
					//Debug
					//System.out.println("new point: " + currentLocation.toString());

					//Add the lowest point to the list
					lowPointList.add(currentLocation);
				}
			}
		}

		//Store the size of the largest basins
		PriorityQueue<Integer> largestBasins = new PriorityQueue<>();


		//Iterate through the lowest points...
		for(WeightedPoint point : lowPointList)
		{
			//A storage place for all points to iterate through
			Deque<WeightedPoint> neighbors = new LinkedList<>();

			//Set of visited points
			Set<WeightedPoint> visited = new HashSet<>();

			//Add the current point to the neighbors list so that its not empty
			neighbors.addFirst(point);

			//As long as the neighbors list isn't empty...
			while(neighbors.size() > 0)
			{
				//Take the first point off the top of the "stack"
				WeightedPoint current = neighbors.removeFirst();

				//Add it to the visited list
				visited.add(current);

				//Get the current point's x and y coordinates
				int xCoord = current.getX();
				int yCoord = current.getY();

				//if the current point isn't the left-most point
				if(xCoord > 0) 
				{
					//Get the point to the left
					WeightedPoint leftNeighbor = mapOfPoints.get(yCoord).get(xCoord - 1);

					//if the left neighbor isn't a wall, and hasn't already been visited
					if(leftNeighbor.getWeight() != 9 && !visited.contains(leftNeighbor)) 
					{
						//Add it to the list of places to check
						neighbors.addLast(leftNeighbor);
					}
				}

				//if the current point isn't the right-most point
				if(xCoord < X_MAX - 1) 
				{
					//Get the point to the right
					WeightedPoint rightNeighbor = mapOfPoints.get(yCoord).get(xCoord + 1);

					//if the right neighbor isn't a wall, and hasn't already been visited
					if(rightNeighbor.getWeight() != 9 && !visited.contains(rightNeighbor))
					{
						//Add it to the list of places to check
						neighbors.addLast(rightNeighbor);
					}
				}

				//if the current point isn't the top-most point
				if(yCoord > 0) 
				{
					//Get the point above
					WeightedPoint upNeighbor = mapOfPoints.get(yCoord - 1).get(xCoord);

					//if the right neighbor isn't a wall, and hasn't already been visited
					if(upNeighbor.getWeight() != 9 && !visited.contains(upNeighbor))
					{
						//Add it to the list of places to check
						neighbors.addLast(upNeighbor);
					}
				}

				//if the current point isn't the bottom-most point
				if(yCoord < Y_MAX - 1) 
				{
					//Get the point below
					WeightedPoint downNeighbor = mapOfPoints.get(yCoord + 1).get(xCoord);

					//if the right neighbor isn't a wall, and hasn't already been visited
					if(downNeighbor.getWeight() != 9 && !visited.contains(downNeighbor))
					{
						//Add it to the list of places to check
						neighbors.addLast(downNeighbor);
					}
				}
			}
			//At this point, all neighbors have been found, and the basin size has been tallied
			//in the variable basinSize. Now we have to check if there are bigger basins.

			if(largestBasins.size() < 3)
			{
				largestBasins.add(visited.size());
			}
			else
			{
				if(largestBasins.peek() < visited.size())
				{
					largestBasins.remove();
					largestBasins.add(visited.size());
				}
			}
		}

		//The largest basins have now been found. 

		//Calculate the final total by multiplying all values in the array largestBasins together
		int finalTotal = 1;
		for(int basinSize : largestBasins)
		{
			finalTotal *= basinSize;
		}

		//Output final total
		System.out.println("Final total: " + finalTotal);
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point to the right is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point to the right
	 */
	private static boolean lowestPointRight(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the x coordinate is already the right-most value,
		// there is no location to check. Return true to save error-checking
		if(xCoord + 1 >= X_MAX) return true;

		//If the weight of the test point is lower than the point to the right
		return testPoint.getWeight() < mapOfPoints.get(yCoord).get(xCoord + 1).getWeight();
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point to the left is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point to the left
	 */
	private static boolean lowestPointLeft(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the x coordinate is already the left-most value,
		// there is no location to check. Return true to save error-checking
		if(xCoord - 1 < 0) return true;

		//If the weight of the test point is lower than the point to the left
		return testPoint.getWeight() < mapOfPoints.get(yCoord).get(xCoord - 1).getWeight();
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point above is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point above
	 */
	private static boolean lowestPointUp(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the y coordinate is already the top-most value,
		// there is no location to check. Return true to save error-checking
		if(yCoord - 1 < 0) return true;

		//If the weight of the test point is lower than the point above
		return testPoint.getWeight() < mapOfPoints.get(yCoord - 1).get(xCoord).getWeight();
	}

	/**
	 * Internal method used for finding the lowest point.
	 * This tests if the point below is larger than the current point.
	 * 
	 * @param testPoint - the point you want to test.
	 * @param mapOfPoints - The full map of all points
	 *
	 * @return boolean - retuns if the testpoint is lower than the point below
	 */
	private static boolean lowestPointDown(WeightedPoint testPoint, ArrayList<ArrayList<WeightedPoint>> mapOfPoints)
	{
		//Debug
		//System.out.println(testPoint);

		//read the x and y coordinates of the test point
		int xCoord = testPoint.getX();
		int yCoord = testPoint.getY();

		//If the y coordinate is already the bottom-most value,
		// there is no location to check. Return true to save error-checking
		if(yCoord + 1 >= Y_MAX) return true;

		//If the weight of the test point is lower than the point below
		return testPoint.getWeight() < mapOfPoints.get(yCoord + 1).get(xCoord).getWeight();
	}
}
