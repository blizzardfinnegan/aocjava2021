package day06;
import java.util.List;

import helpers.*;

/**
 * Lanternfish problem for Advent of Code
 * 
 * @author Blizzard Finnegan
 */
public class DaySixPartOne 
{
  public static void main(String[] args) 
  { 
    //Constant; the last day for the iteration
    final int FINAL_DAY = 80; 

    //Import file
    List<String> fileImport = FileRead.readFile("day06/data/input.txt");
  
    //Parse file into long array (long is necessary for part 2)
    //Take the only string out of the imported file, and split by comma
    String fileLine = fileImport.remove(0);
    String[] lineSplit = fileLine.split(",");

    //For the below arrays, the index represents the age of a lanternfish.
    //Working array
    long[] currentFishTime = new long[9];
    //Next array
    long[] nextFishTime = new long[9];

    //Parse string array into long array
    for(String fishString : lineSplit)
    {
      int fishInt = Integer.parseInt(fishString);
      currentFishTime[fishInt]++;
    }
  
    //Iterate for the declared number of days
    for(int day = 0; day < FINAL_DAY; day++)
    {
      //Age all fish by one
      for(int i = 0; i < 8; i++)
      {
        nextFishTime[i] = currentFishTime[i+1];
      }
  
      //Fish reproduce every 7 days; newly spawned fish take 8 days to start reproducing
      nextFishTime[8] = currentFishTime[0];
      nextFishTime[6] += currentFishTime[0];
  
      //Shift working array into storage array
      for(int i = 0; i < 9; i++)
      {
        currentFishTime[i] = nextFishTime[i];
      }
    }
  
    //Calculate the total number of fish
    long total = 0;
    for(long fish : currentFishTime)
    {
      total += fish;
    }

    //Print the final total
    System.out.println("Total lanternfish: " + total);
  }
}
