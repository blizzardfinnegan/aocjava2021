package day04;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

import helpers.*;

/**
 * DayFourPartTwo of Advent of Code 2021
 * 
 * @author Blizzard Finnegan
 */
public class DayFourPartTwo
{
  //Class-wide Arraylist for the movelist and their order
  private static ArrayList<Integer> moveOrder = new ArrayList<>();

  /**
   * 
   * @param board - An arraylist of points on the bingo board
   * @return how many moves it takes to win. Returns -1 if it can't be won.
   */
  private static int boardTest(ArrayList<Integer> board)
  {
    //Creates a boolean arraylist of the same size as board
    ArrayList<Boolean> boardFill = new ArrayList<>();

    //Fill the board with false, as nothing's been tested yet
    for(Integer value : board)
    {
      boardFill.add(false);
    }

    //iterate through the move list
    for(int moveCount = 0; moveCount < moveOrder.size(); moveCount++)
    {
      //Get the next move in the game
      int currentMove = moveOrder.get(moveCount);

      //If the move is on the board...
      if(board.contains(currentMove))
      {
        //Turn the same location on the boardfill to also be true
        boardFill.set(board.indexOf(currentMove), true);
      }

      //Check the board for bingos
      for(int i = 0; i < 5; i++)
      {
        //Column check
        if(boardFill.get(i) && boardFill.get(i+5) && boardFill.get(i+10) && boardFill.get(i+15) && boardFill.get(i+20))
        {
          return moveCount;
        }
        //Row check
        else if(boardFill.get(5*i) && boardFill.get(5*i+1) && boardFill.get(5*i+2) && boardFill.get(5*i+3) && boardFill.get(5*i+4))
        {
          return moveCount;
        }
      }
    }

    //If it manages to finish the entire above loop without returning, it can't be a winning board
    return -1;
  }

  /**
   * Internal function used to help turn strings into a board
   * @param stringBoard - the 5 lines of a board, in string format (direct from import)
   * @return One board, styled correctly
   */
  private static ArrayList<Integer> setFiller(ArrayList<String> stringBoard)
  {
    ArrayList<Integer> output = new ArrayList<>();
    //Iterate through list of strings
    for(String line : stringBoard)
    {
      //Split the string
      String[] splitLine = line.split(" ");
      //Iterate through the string array
      for(String move : splitLine)
      {
        //Add to the output, as long as its a real string
        if(!move.equals(""))
        {
          output.add(Integer.parseInt(move));
        }
      }
    }
    return output;
  }
  public static void main(String[] args) 
  {
    //Read input file
    List<String> fileInput = FileRead.readFile("day04/data/input.txt");

    //Create the maximum move count; default it to a number that will be quickly changed
    int maxMoveCount = 0;

    //Create a hashset of boards
    HashSet<ArrayList<Integer>> setOfBoards = new HashSet<>();

    //Split the first line of the file based on commas
    String[] stringMoves = fileInput.get(0).split(",");

    //Remove the first line and the blank line that follows 
    //from the imported file
    fileInput.remove(0);
    fileInput.remove(0);

    //Add a blank line to the end of the file so below processing works
    fileInput.add("");
    

    //Turn string array into ArrayList<Integer>
    for (String move : stringMoves) 
    {
      moveOrder.add(Integer.parseInt(move));
    }

    ArrayList<String> unparsedBoard = new ArrayList<>();
    //Iterate through the remainder of the file
    for(String line : fileInput)
    {
      //ArrayList<String> unparsedBoard = new ArrayList<>();
      //If the line has contents...
      if(!line.equals(""))
      {
        unparsedBoard.add(line);
      }

      //If the line doesn't have contents
      else
      {
        //... and the board isn't empty, add it to the set, and clear current for the next
        if(unparsedBoard.size() > 0)
        {
          setOfBoards.add(setFiller(unparsedBoard));
        }
        unparsedBoard.clear();
      }
    }

    //Make a spot to store the winning board
    ArrayList<Integer> winningBoard = new ArrayList<>();

    //Iterate through all boards
    int boardMoveCount;
    for(ArrayList<Integer> board : setOfBoards)
    {
      //Using the boardtest function, find out how many moves it takes to win
      boardMoveCount = boardTest(board);

      //If the board can be won, but takes less moves than the current maximum
      //Make the max move count this movecount, and overwrite the winning board
      if(boardMoveCount > 0 && boardMoveCount > maxMoveCount)
      {
        maxMoveCount = boardMoveCount;
        winningBoard = board;
      }
    }

    System.out.println(winningBoard);
    //Almost done...
    int finalTotal = 0;

    //Half-step list of things to remove from the winning board
    //  (Final score is based on remaining moves that can be made on the 
    //   board, summed, multiplied by the winning move)
    ArrayList<Integer> removalList = new ArrayList<>();

    //Iterate through the movelist up to the winning move
    for(int i = 0; i <= maxMoveCount; i++)
    {
      //If the board has that move, add the same move to the removelist
      if(winningBoard.contains(moveOrder.get(i)))
      {
        removalList.add(moveOrder.get(i));
      }
    }

    //Clear the winning board of every move made
    winningBoard.removeAll(removalList);


    //For every move not made on the winning board, sum them together
    for(Integer remainingValue : winningBoard)
    {
      finalTotal += remainingValue;
    }

    //Multiply the above sum by the winning move
    finalTotal *= moveOrder.get(maxMoveCount);

    //Print
    System.out.println("Final score: " + finalTotal);
  }
}
