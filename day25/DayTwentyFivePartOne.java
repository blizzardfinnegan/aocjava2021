package day25;

import helpers.*;

/**
 * Day 25 Part 1 of Advent of Code 2021:
 * Imported file is a map of the following characters:
 *
 * >
 * v
 * .
 *
 * > characters move right, v characters move down. '.' characters are blank. 
 * > characters move before v characters.
 * When a character hits its respective edge, if the space on the opposite edge is
 * open, on the next iteration it will roll over to the opposite edge.
 *
 * Print how many iterations it takes to stop all characters from moving.
 * 
 * The test input takes 58 iterations.
 *
 * @author Blizzard Finnegan
 */
public class DayTwentyFivePartOne
{
	public static void main(String[] args)
	{
	}
}
