package day01;

import helpers.SimpleChecks;
import helpers.FileRead;
import java.util.List;


/**
 * DayOne, PartTwo of the AdventOfCode challenge
 *
 * @author Blizzard Finnegan
 */
public class DayOnePartTwo
{
  public static void main (String[] args) throws Error
  {
    //Necessary number storage
    int currentValue = 0;
    int middleValue = 0;
    int lastValue = 0;

    //This is now -1 to account for an off-by-one error
    int decreaseCount = -1;

    //New number storage for part2
    int lastAverage = 0;
    int currentAverage = 0;

    //Import the given input file
    List<String> fileInput = FileRead.readFile("day01/data/input.txt");

    //Iterate through the file; for each line in the file...
    for(String line : fileInput)
    {
      //If the line isn't an integer, then it isn't valid for this challenge. Error out.
      if(!SimpleChecks.isInteger(line)) throw new Error("Wrong file!");

      //If it is an integer, set the current value to it
      currentValue = Integer.parseInt(line);

      //Set currentAverage based on 3 past values
      currentAverage = currentValue + middleValue + lastValue;

      //If this isn't the first value, but is more than the last, increment the decent counter
      if(lastValue != 0 && lastAverage != 0 && lastAverage < currentAverage) decreaseCount++;

      //Cycle all values from current to the next in the chain
      lastAverage = currentAverage;
      lastValue = middleValue;
      middleValue = currentValue;
    }

    //Print the total
    System.out.println("Total Depth Change: " + decreaseCount);
  }
}
