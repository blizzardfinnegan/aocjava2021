package day01;

import helpers.SimpleChecks;
import helpers.FileRead;
import java.util.List;


/**
 * DayOne, PartOne of the AdventOfCode challenge
 *
 * @author Blizzard Finnegan
 */
public class DayOnePartOne
{
  public static void main (String[] args) throws Error
  {
    //Necessary number storage
    int currentValue = 0;
    int lastValue = 0;
    int decreaseCount = 0;

    //Import the given input file
    List<String> fileInput = FileRead.readFile("day01/data/input.txt");

    //Iterate through the file; for each line in the file...
    for(String line : fileInput)
    {
      //If the line isn't an integer, then it isn't valid for this challenge. Error out.
      if(!SimpleChecks.isInteger(line)) throw new Error("Wrong file!");

      //If it is an integer, set the current value to it
      currentValue = Integer.parseInt(line);

      //If this isn't the first value, but is more than the last, increment the decent counter
      if(lastValue != 0 && lastValue < currentValue) decreaseCount++;

      //Set the last value to the current value, as the loop will now repeat
      lastValue = currentValue;
    }

    //Print the total
    System.out.println("Total Depth Change: " + decreaseCount);
  }
}
