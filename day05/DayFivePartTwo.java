package day05;
import java.util.List;
import  java.util.ArrayList;

import helpers.*;

public class DayFivePartTwo
{
  public static void main(String[] args) 
  {
    //Read input file
    List<String> fileInput = FileRead.readFile("day05/data/input.txt"); 

    //Storage array for all vectors
    ArrayList<MapVector> listOfVectors = new ArrayList<>();

    //Map of all locations. Not initialized yet; we do that later
    int[][] map;
    
    //Misc. working variables
    int overlapCount = 0;
    int mapXMax = 0;
    int mapYMax = 0;

    //Each line in the input file is given as follows:
    // xcoord,ycoord -> xcoord,ycoord
    //The below iterates through the imported file, parses each line, and adds them to the list of vectors
    // for each line...
    for(String line : fileInput)
    {
      //Split on arrow
      String[] halfParsed = line.split(" -> ");

      //Debug
      //System.out.println(Arrays.toString(halfParsed));

      //Split on the comma
      String[] startString = halfParsed[0].split(",");
      String[] endString = halfParsed[1].split(",");

      //Convert string to int, create pair out of it
      IntPair startPair = new IntPair(Integer.parseInt(startString[1]), Integer.parseInt(startString[0]));
      IntPair endPair = new IntPair(Integer.parseInt(endString[1]), Integer.parseInt(endString[0]));

      //Add new MapVector to listofvectors
      listOfVectors.add(new MapVector(startPair, endPair));
    }

    //Iterate over list
    for(MapVector currentVector : listOfVectors)
    {
      //Reset max x and y based on max in vector
      mapXMax = currentVector.getXMax() > mapXMax ? currentVector.getXMax() : mapXMax;
      mapYMax = currentVector.getYMax() > mapYMax ? currentVector.getYMax() : mapYMax;
    }

    //Initialize map to the right size
    map = new int[mapXMax+1][mapYMax+1];

    //Iterate through list of vectors...
    for(MapVector currentVector : listOfVectors)
    {
      int x = 0;
      int y = 0;
      //Add vector to the map, depending on direction
      switch (currentVector.getDirection()) 
      {
        //If the vector is vertical
        case VERTICAL:
          //Get the X value of the vector (since both x values are the same, this is shorter)
          x = currentVector.getXMax();

          //Parse vector correctly
          //If the vector is pointing up
          if(currentVector.pointingUp())
          {
            //Iterate from bottom to top, adding 1 to each point on the map along the way
            for(y = currentVector.getStart().getY(); y <= currentVector.getEnd().getY(); y++)
            {
              map[x][y]++;
            }
          }
          //If the vector is pointing down
          else
          {
            //Iterate from top to bottom
            for(y = currentVector.getStart().getY(); y >= currentVector.getEnd().getY(); y--)
            {
              map[x][y]++;
            }
          }
          break;
      
        //If the vector is
        case HORIZONTAL:
          //See explanation in vertical; All x and y values here are simply swapped from above
          y = currentVector.getYMax();
          if(currentVector.pointingRight())
          {
            for(x = currentVector.getStart().getX(); x <= currentVector.getEnd().getX(); x++)
            {
              map[x][y]++;
            }
          }
          else
          {
            for(x = currentVector.getStart().getX(); x >= currentVector.getEnd().getX(); x--)
            {
              map[x][y]++;
            }
          }
          break;

        //Uses the pointingQuadrant function to determine which way to iterate
        case DIAGONAL:
          switch (currentVector.pointingQuadrant()) {
            case 1:
              x = currentVector.getStart().getX();
              y = currentVector.getStart().getY();
              while(x <= currentVector.getEnd().getX() && y <= currentVector.getEnd().getY())
              {
                map[x][y]++;
                x++;
                y++;
              }
              break;

            case 2:
              x = currentVector.getStart().getX();
              y = currentVector.getStart().getY();
              while(x >= currentVector.getEnd().getX() && y <= currentVector.getEnd().getY())
              {
                map[x][y]++;
                x--;
                y++;
              }
              break;

            case 3:
              x = currentVector.getStart().getX();
              y = currentVector.getStart().getY();
              while(x >= currentVector.getEnd().getX() && y >= currentVector.getEnd().getY())
              {
                map[x][y]++;
                x--;
                y--;
              }
              break;

            case 4:
              x = currentVector.getStart().getX();
              y = currentVector.getStart().getY();
              while(x <= currentVector.getEnd().getX() && y >= currentVector.getEnd().getY())
              {
                map[x][y]++;
                x++;
                y--;
              }
              break;

            default:
              System.out.println("Error in Diagonal Parsing!");
              break;
          }
          break;

        //If there's a problem, tell me
        //Surpress for final output, as the output on partOne will be LONG
        default:
          System.out.println("Error in vector parsing! Vector: " + currentVector.toString());
          break;
      }
    }

    //Step through the map
    for(int x = 0; x < map.length; x++)
    {
      for(int y = 0; y < map[0].length; y++)
      {
        //Debug; ONLY USE ON TEST DATASET
        //if(map[x][y] == 0) System.out.print(".\t");
        //else System.out.print(map[x][y] + "\t");

        //If the point has an overlap, increment the count
        if(map[x][y] > 1) overlapCount++;
      }
      //Debug; part of above
      //System.out.println();
    }

    System.out.println("Overlap count: " + overlapCount);
  }  
}
