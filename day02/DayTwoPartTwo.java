package day02;

import java.util.List;

import helpers.*;

public class DayTwoPartTwo
{
  public static void main (String[] args) throws Error
  {
    //Main storage variables
    int horizontalPosition = 0;
    int verticalPosition = 0;
    int angle = 0;

    //Parse the input file
    List<String> fileInput = FileRead.readFile("day02/data/input.txt");
    //For every line in the file...
    for(String line : fileInput)
    {
      //Break the line into two parts
      //The direction is stored in index 0, the distance is stored in index 1
      String[] splitString = line.split(" ");

      //Error-checking
      if(!SimpleChecks.isInteger(splitString[1])) throw new Error("Wrong input file");

      //Parse the distance into an integer
      int parsedInt = Integer.parseInt(splitString[1]);

      //Based on direction, change the horizontal or vertical position
      switch (splitString[0].charAt(0))
      {
        //If forward, move forward and up/down
        case 'f':
          horizontalPosition += parsedInt;
          verticalPosition += angle * parsedInt;
          break;
        //If down, change angle
        case 'd':
          angle += parsedInt;
          break;
        //If up, change angle
        case 'u':
          angle -= parsedInt;
          break;
        //Error-checking
        default:
          throw new Error("also wrong file");
      }
    }
    //Print final answer (multiply horizontal by vertical)
    System.out.println("Final depth: " + (horizontalPosition * verticalPosition));
  }
}
