package helpers;

/**
 * Helper class for working with a mathematical static vector using contained IntPairs.
 * Once created, all values are read-only.
 * 
 * @author Blizzard Finnegan
 */
public class MapVector {

  //The starting point of the vector; the "tail"
  private final IntPair start;

  //The ending point of the vector; the "tip"
  private final IntPair end;

  /**
   * Deault constructor for MapVector
   * @param start - the "tail" of the vector
   * @param end - the "tip" of the vector
   */
  public MapVector(IntPair start, IntPair end) {this.start = start; this.end = end;}

  /**
   * Getter for the "tail" of the vector
   * @return IntPair of the starting point
   */
  public IntPair getStart() { return this.start; }

  /**
   * Getter for the "tip" of the vector
   * @return IntPair of the ending point
   */
  public IntPair getEnd() { return this.end; }

  @Override
  public boolean equals(Object obj) 
  {
    if(!(obj instanceof MapVector)) return false;
    MapVector castObj = (MapVector)obj;
    return (this.start.equals(castObj.start)) && (this.end.equals(castObj.end));
  }

  @Override
  public String toString() 
  {
    return String.format("[%s -> %s]", this.start.toString(), this.end.toString());
  }

  /**
   * Getter for the maximum coordinate in the vector.
   * @return int The max coordinate of the vector
   */
  public int getMax()
  {
    return start.getMax() >= end.getMax() ? start.getMax() : end.getMax() ;
  }

  /**
   * Compares the X value in the two points of the vector to get the maximum value.
   * @return int of the max X value
   */
  public int getXMax()
  {
    return start.getX() >= end.getX() ? start.getX() : start.getX();
  }

  /**
   * Compares the Y value in the two points of the vector to get the maximum value.
   * @return int of the max Y value
   */
  public int getYMax()
  {
    return start.getY() >= end.getY() ? start.getY() : start.getY();
  }

  /**
   * Getter for the direction of the vector
   * @return A value from Direction
   */
  public Direction getDirection()
  {
    if(start.getX() == end.getX() && start.getY() == end.getY()) return Direction.POINT;
    else if(start.getX() == end.getX()) return Direction.VERTICAL;
    else if(start.getY() == end.getY()) return Direction.HORIZONTAL;
    else return Direction.DIAGONAL;
  }

  /**
   * Determines whether a horizontal vector is pointing right or left
   * @return boolean - pointing right is true; pointing left (or vertically/point) is false
   */
  public boolean pointingRight()
  {
    return start.getX() < end.getX();
  }

  /**
   * Determines whether a horizontal vector is pointing up or down
   * @return boolean - pointing right is true; pointing left (or vertically/point) is false
   */
  public boolean pointingUp()
  {
    return start.getY() < end.getY();
  }

  /**
   * Determines the quadrant a diagonal vector is pointing to
   * @return -1 if not diagonal; 1 if pointed up-and-right, 2 if pointed up-and-left, 3 if down-and-left, 4 if down-and-right
   */
  public int pointingQuadrant()
  {
    if(this.getDirection() != Direction.DIAGONAL) return -1;
    else if(this.pointingRight() && this.pointingUp()) return 1;
    else if(!this.pointingRight() && this.pointingUp()) return 2;
    else if(!this.pointingRight() && !this.pointingUp()) return 3;
    else if(this.pointingRight() && !this.pointingUp()) return 4;
    else return -1;
  }

  /**
   * Quick enum for possible directions for a vector. 
   */
  public enum Direction
  {
    POINT,
    HORIZONTAL,
    VERTICAL,
    DIAGONAL;
  }
}
