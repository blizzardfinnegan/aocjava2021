package helpers;

/**
 * Helper class containing a standard Cartesian point.
 * These values are read-only once written.
 * 
 * @author Blizzard Finnegan
 */
public class IntPair {
  //X value of the coordinate
  private final int x;
  //Y value of the coordinate
  private final int y;

  /**
   * Default constructor
   * @param x - The x value of the point
   * @param y - The y value of the point
   */
  public IntPair(int x, int y) { this.x = x; this.y = y; }

  /**
   * Getter for the x-value of the point
   * @return int, x value
   */
  public int getX() { return this.x; }

  /**
   * Getter for the y-value of the point
   * @return int, y value
   */
  public int getY() { return this.y; }

  @Override
  public boolean equals(Object obj) 
  {
    if(!(obj instanceof IntPair)) { return false; }
    IntPair castObj = (IntPair)obj;
    return (this.x == castObj.x) && (this.y == castObj.y);
  }

  @Override
  public String toString() 
  {
    return String.format("(%s,%s)", this.x, this.y);
  }

  /**
   * Getter for the max value of the pair
   * @return
   */
  public int getMax()
  {
    return x >= y ? x : y;
  }
}
