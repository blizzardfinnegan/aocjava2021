package day12;

import helpers.*;
import graphs.*;

/**
 * Day 12 Part 1 of Advent of Code 2021:
 *
 * File contains undirected edges, in the format:
 * point-point
 *
 * Points are either named "start", "end", or a two-letter point name. 
 * Points can either be lowercase or uppercase.
 * 
 * Find the number of paths that visit any one lowercase point at most once.
 *
 * @author Blizzard Finnegan
 */
public class DayTwelvePartOne
{
	public static void main(String[] args)
	{
		/*
		 * import file
		 * 
		 * new adjacencygraph
		 *
		 * Add two vertexes, start and end
		 *
		 * for each line in the file
		 *		split at hyphen
		 *		for each string in the split
		 *				if the string isn't in the graph, add it
		 *		connect the two points in the current line
		 *
		 * backtracking... second file:
		 * implements configuration
		 * override tostring
		 *
		 * override isValid: 
		 *		iterate through configuration...
		 *				if a lowercase point is listed twice return false
		 *    return true
		 * override getSuccessors: return all neighbors
		 * override isGoal: if first value in config is 'start' and last value in config is 'end'
		 *
		 * Modify backtracker file to return amount of isGoal configurations
		 */
	}
}
