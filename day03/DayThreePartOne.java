package day03;

import java.util.Arrays;
import java.util.List;

import helpers.*;

/**
 * DayThreePartOne of Advent of Code
 *
 * @author Blizzard Finnegan
 */
public class DayThreePartOne
{
  public static void main (String[] args) throws Error
  {
    int[] numOfZeroes;
    int[] numOfOnes;

    //Read input file
    List<String> fileInput = FileRead.readFile("day03/data/input.txt");

    //Array initialization
    String firstLine = fileInput.get(0);
    numOfZeroes = new int[firstLine.length()];
    numOfOnes = new int[firstLine.length()];

    //Fill arrays with null data
    for(int i = 0; i < firstLine.length(); i++)
    {
      numOfZeroes[i] = 0;
      numOfOnes[i] = 0;
    }

    //For every line in the file...
    for(String line : fileInput)
    {
      //Change the line of bits into a chararray
      char[] bits = line.toCharArray();

      //iterate over the line
      for(int i = 0; i < bits.length; i++)
      {
        //If the bit is a 0, increment the number of 0s in that index
        //If 1, increment 1s
        switch (bits[i])
        {
          case '0':
            numOfZeroes[i]++;
            break;
          case '1':
            numOfOnes[i]++;
            break;

          //If a bit is neither a 0 or a 1, its the wrong file. Error out.
          default:
            throw new Error("Wrong file");
        }
      }
    }

    int mostCommon = 0;
    int leastCommon = 0;
    //Debug print statements
    System.out.println(Arrays.toString(numOfOnes));
    System.out.println(Arrays.toString(numOfZeroes));

    //Iterate through the now-filled numofzeroes array
    for(int i = numOfZeroes.length - 1; i >= 0;i--)
    {
      /*
       * The way that iterations are done, the numofzeroes array is LSB rather than MSB
       * This line swaps the location of bits back to the correct location for parsing
       */
      int binaryIndex = (numOfZeroes.length - 1) - i;

      //If there's more ones, add the current power to there. If there's more zeroes, add the current power there.
      if(numOfOnes[i] > numOfZeroes[i]) mostCommon += Math.pow(2,binaryIndex);
      else leastCommon += Math.pow(2,binaryIndex);
    }

    //Print the final outcome
    System.out.println("Power consumption: " + (mostCommon * leastCommon));
  }
}
