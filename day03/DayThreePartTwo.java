package day03;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

import helpers.*;

public class DayThreePartTwo
{
  private static int byteSize;
  private static String commonalityFilter(List<String> commonList, boolean mostFilter)
  {
    //Iterate through each bit individually
    for(int i = 0; i < byteSize; i++)
    {
      //A trueness indicator. Positive means more 1s, negative means more 0s
      int trueness = 0;

      //List to store all the values to remove
      List<String> removeList = new ArrayList<>();

      //If its just one string in the list, return it
      if(commonList.size() == 1) return commonList.get(0);

      //For each line in the list...
      for(String line : commonList)
      {
        //If the line contains a 1 at the current index, make the trueness more tree
        //otherwise, make it less true
        trueness += (line.charAt(i) == '1') ? 1 : -1;
      }

      //If there's more 1s than 0s (or equal)
      if(trueness >= 0)
      {
        
        //Trim based on boolean check
        for(String line : commonList)
        {
          //If we're filtering for the most-common, remove the 0s
          //If we're filtering for the least-common, remove the 1s
          if((mostFilter && line.charAt(i) == '0') || (!mostFilter && line.charAt(i) == '1'))
          {
            removeList.add(line);
          }
        }
      }
      //If there's more 1s than 0s (or equal)
     else
      {

        //Trim based on boolean check
        for(String line : commonList)
       {
          //If we're filtering for the most-common, remove the 1s
          //If we're filtering for the least-common, remove the 0s
          if((mostFilter && line.charAt(i) == '1') || (!mostFilter && line.charAt(i) == '0'))
          {
            removeList.add(line);
          }
        }        
      }
      //Once all bad strings are collected, delete them
      commonList.removeAll(removeList);
      //debug statement
      //System.out.println(commonList);
    }
    //Return the final value in the list
    return commonList.get(0);
  }
  public static void main (String[] args) throws Error
  {
    //read in from file
    List<String> fileInput = FileRead.readFile("day03/data/input.txt");
    List<String> mostCommonList = new ArrayList<>(fileInput);
    List<String> leastCommonList = new ArrayList<>(fileInput);

    //Array initialization
    String firstLine = fileInput.get(0);
    byteSize = firstLine.length();

    String mostCommonLine = commonalityFilter(mostCommonList, true);
    String leastCommonLine = commonalityFilter(leastCommonList, false);

    int mostCommonValue = Integer.parseInt(mostCommonLine,2);
    int leastCommonValue = Integer.parseInt(leastCommonLine, 2);

    System.out.println("Life support rating: " + (mostCommonValue * leastCommonValue));
  }
}
