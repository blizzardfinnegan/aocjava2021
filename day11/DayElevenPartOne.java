package day11;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import helpers.*;

/**
 * Day 11 Part 1 of Advent of code:
 *
 * The imported file is a grid of numbers from 0 to 9. There are no separators.
 * Each point is a counter, that counts up. When the counter rolls over, all numbers
 * surrounding the rolling over number also increment. Here is an example before 
 * and after grid:
 *
 * Day 0
 * 11111
 * 19991
 * 19191
 * 19991
 * 11111
 *
 * Day 1
 * 34543
 * 40004
 * 50005
 * 40004
 * 34543
 *
 * Day 2
 * 45654
 * 51115
 * 61116
 * 51115
 * 45654
 *
 * Find the number of times that the counter rolls over.
 *
 * testInput2 is for initial testing of the functionality. Should only run for 2 days.
 * testInput1 is a full test for 100 days. This should output to 1656 as the final value.
 *  
 * @author Blizzard Finnegan
 */
public class DayElevenPartOne
{
	private static ArrayList<ArrayList<CounterPoint>> mapOfPoints = new ArrayList<>();
	private static int X_MAX = 0;
	private static int Y_MAX = 0;

	private static void incrementNeighbors(CounterPoint currentPoint)
	{
		if(currentPoint.getCount() != 10) return;
		List<IntPair> neighbors = currentPoint.getNeighbors(X_MAX,Y_MAX);
		for(IntPair neighbor : neighbors)
		{
			CounterPoint counterNeighbor = mapOfPoints.get(neighbor.getY()).get(neighbor.getX());
			counterNeighbor.increment();
			if(counterNeighbor.getCount() == 10)
			{
				incrementNeighbors(counterNeighbor);
			}
		}
	}

	public static void main(String[] args)
	{
		final int DAY_COUNT = 100;
		int rolloverCount = 0;

		/*
		 * Iterate through file... for each line...
		 *		for each character in the line...
		 *				make a new counter point, save it to an arraylist
		 *		save arraylist to map
		 */

		//File import
		List<String> fileImport = FileRead.readFile("day11/data/input.txt");

		int yCounter = 0;
		for(String line : fileImport)
		{
			ArrayList<CounterPoint> parsedLine = new ArrayList<>();
			int xCounter = 0;
			char[] charLine = line.toCharArray();
			for(char point : charLine)
			{
				int initialCount = Character.getNumericValue(point);
				parsedLine.add(new CounterPoint(new IntPair(xCounter,yCounter),initialCount));
				xCounter++;
			}
			mapOfPoints.add(parsedLine);
			X_MAX = xCounter;
			yCounter++;
		}
		Y_MAX = yCounter;

		//Debug print of map
		//for(ArrayList<CounterPoint> row : mapOfPoints)
		//{
		//	for(CounterPoint currentPoint : row)
		//	{
		//		System.out.print(currentPoint.getCount());
		//	}
		//	System.out.println();
		//}
		//System.out.println("-----------------");

		/*
		 * iterate for the number of days...
		 *		clear nextMap
		 *		iterate through current map...
		 *			increment the current point's counter
		 *			if(current point counter > 9)
		 *					increment all neighbors
		 *					check neighbors
		 *					if neighbor counter > 9
		 *							etc...
		 *		iterate through current map...
		 *      if current point rolls over, increment the rollover counter 
		 *
		 * Print rollover counter
		 */
		for(int i = 0; i < DAY_COUNT; i++)
		{
			HashSet<IntPair> visited = new HashSet<>();
			for(ArrayList<CounterPoint> row : mapOfPoints)
			{
				for(CounterPoint currentPoint : row)
				{
					currentPoint.increment();
					if(currentPoint.getCount() == 10)
					{
						incrementNeighbors(currentPoint);
					}
				}
			}

			for(ArrayList<CounterPoint> row : mapOfPoints)
			{
				for(CounterPoint currentPoint : row)
				{
					if(currentPoint.rollOver()) rolloverCount++;
				}
			}

			//Debug print of map
			//if(i % 10 == 0)
			//{
			//	for(ArrayList<CounterPoint> row : mapOfPoints)
			//	{
			//		for(CounterPoint currentPoint : row)
			//		{
			//			if(currentPoint.getCount() == 0) System.out.print("*");
			//			else System.out.print(currentPoint.getCount());
			//		}
			//		System.out.println();
			//	}
			//	System.out.println("-----------------");
			//}
		}

		//Debug print of map
		//for(ArrayList<CounterPoint> row : mapOfPoints)
		//{
		//	for(CounterPoint currentPoint : row)
		//	{
		//		System.out.print(currentPoint.getCount());
		//	}
		//	System.out.println();
		//}
		//System.out.println("-----------------");

		System.out.println("Final rollover count: " + rolloverCount);
	}

}
