package day22;

import helpers.*;
/**
 * Day 22 Part 1 of Advent of Code 2021:
 * Imported file is a series of on and off commands for a triple-nested array of booleans.
 * All values start FALSE. The import file is of the following format:
 *
 * on x=11..13,y=11..13,z=11..13
 *
 * The on value can also be "off". For now, ignore all values outside the range -50,50
 *
 * The test input should show 590784 on values.
 *
 * @author Blizzard Finnegan
 */
public class DayTwentyTwoPartOne
{
	public static void main(String[] args)
	{
		/**
		 * Import file
		 *
		 * Iterate through each line in the file
		 *		split the line by " "
		 *		setValue = line[0].equals("on") ? true : false;
		 *		split the second half of the line by ","
		 *		for(string in second half array)
		 *				split at "="
		 *				split second half at ".."
		 *				switch(first half; x,y,z) set max and min
		 *		iterate through array by defined max and min
		 *				set value according to setValue
		 *
		 * iterate through the array from -50 to +50 in each direction
		 *		if true add 1
		 *
		 * output sum
		 */
	}
}
