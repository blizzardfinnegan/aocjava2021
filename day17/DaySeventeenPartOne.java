package day17;

import helpers.*;

/**
 * Day 17 Part 1 of Advent of Code 2021:
 *
 * Imported file contains the following line:
 * target area: x=xmin..xmax, y=ymin..ymax
 *
 * These points represent the corners of the target area. Your job is to fire a 
 *	projectile from (0,0), and land in the target, while making the projectile go
 *	as high as possible. 
 *
 * Projectiles move like in steps. Each step increments the position by its respective 
 *	velocity. The x velocity moves towards 0 by 1 each step, while the y velocity always 
 *	decreases by 1 each step.
 *
 * Output the max height the projectile can hit. 
 *
 * The test input (x=20..30, y=-10..-5) should output 45.
 *
 * @author Blizzard Finnegan
 */
public class DaySeventeenPartOne
{
	public static void main(String[] args)
	{
		/*
		 * import file
		 *
		 * split file by spaces. 
		 * remove the first string, twice
		 * split remaining strings by ".."
		 * remove x= and y=
		 * save as individual variables (X_MAX, Y_MAX, X_MIN, Y_MIN)
		 *
		 * xcounter = 0
		 * ycounter = 0
		 * maxHeight = 0
		 *
		 * while true
		 *	loopMax = 0
		 *	ycounter = 0
		 * 	if xcounter(factorial but addition) < X_MIN, xcounter++; continue
		 * 	//factorial but addition is (n^+n)/2
		 *		while true
		 *				temp = balisticTrajectory(xcounter,ycounter)
		 *				if temp == -1 && loopMax == 0 ycounter++;
		 *				else if temp >= loopMax overwrite max height; ycounter++;
		 *				else break; //the trajectory is now too high, we're overshooting
		 *	if loopMax >= maxHeight, overwrite max height
		 *  else break;
		 *
		 * print maxHeight
		 */
	}
}
