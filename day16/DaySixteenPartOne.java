package day16;

import java.util.ArrayList;
import java.util.List;

import helpers.*;

/**
 * Day 16 Part 1 of Advent of Code 2021:
 *
 * Imported file is a single line of hexadecimal characters, which in total is a 
 * data packet. This packet contains several sub-packets, all defined by standards:
 *
 * The outermost packet is likely padded with 0s. Any zeroes at the end not 
 *		delegated to a subpacket can be ignored.
 *
 * The first 3 bits of each packet (and sub-packet) are the version number.
 *
 * The next 3 bits represent the type ID. ID 4, represents a number, any other ID represents
 *		an calculation on the subpackets. These will (probably) be addressed in part 2.
 *
 *		If the packet is a number, the rest of the packet is in 5-bit chunks. 
 *				If the chunk starts with a 1, there is another chunk following. The remaining 
 *					4 bits in the chunk should be parsed as part of a number.
 *				If the chunk starts with a 0, it is the last chunk. 
 *
 *				Once all chunks have been parsed, the number can be created, and translated back 
 *					into decimal.
 *
 *		If the packet is not a number, it contains sub-packets.
 *				The first bit after the type ID indicates the length of the next chunk.
 *						A 1 indicates a packet size chunk of length 11.
 *						A 0 indicates a packet size chunk of length 15.
 *
 *				The next x bits (as described above) represent the total length of the packet.
 *				
 *				The next y bits (described in the previous line) are sub-packets, and need to 
 *					be parsed recursively.
 *
 * Parse the imported file, add up all version numbers, and print the result.
 *
 * Test input file 1 should output 16.
 * Test input file 2 should output 12.
 * Test input file 3 should output 23.
 * Test input file 4 should output 31.
 *
 * @author Blizzard Finnegan
 */
public class DaySixteenPartOne
{
	private static int boolToInt(List<Boolean> chunk)
	{
		int output = 0;
		for(Boolean bit : chunk)
		{
			if(bit.booleanValue()) output++;
			output = output << 1;
		}
		return output;
	}

	private static int boolToInt(List<Boolean> chunk, int initialValue)
	{
		int output = initialValue;
		for(Boolean bit : chunk)
		{
			if(bit.booleanValue()) output++;
			output = output << 1;
		}
		return output;
	}

	private static ArrayList<Boolean> hexToBool(char hexValue)
	{
		ArrayList<Boolean> output = new ArrayList<>();
		switch(hexValue)
		{
			case '0':
				output.add(false);
				output.add(false);
				output.add(false);
				output.add(false);
				break;
			case '1':
				output.add(false);
				output.add(false);
				output.add(false);
				output.add(true);
				break;
			case '2':
				output.add(false);
				output.add(false);
				output.add(true);
				output.add(false);
				break;
			case '3':
				output.add(false);
				output.add(false);
				output.add(true);
				output.add(true);
				break;
			case '4':
				output.add(false);
				output.add(true);
				output.add(false);
				output.add(false);
				break;
			case '5':
				output.add(false);
				output.add(true);
				output.add(false);
				output.add(true);
				break;
			case '6':
				output.add(false);
				output.add(true);
				output.add(true);
				output.add(false);
				break;
			case '7':
				output.add(false);
				output.add(true);
				output.add(true);
				output.add(true);
				break;
			case '8':
				output.add(true);
				output.add(false);
				output.add(false);
				output.add(false);
				break;
			case '9':
				output.add(true);
				output.add(false);
				output.add(false);
				output.add(true);
				break;
			case 'A':
				output.add(true);
				output.add(false);
				output.add(true);
				output.add(false);
				break;
			case 'B':
				output.add(true);
				output.add(false);
				output.add(true);
				output.add(true);
				break;
			case 'C':
				output.add(true);
				output.add(true);
				output.add(false);
				output.add(false);
				break;
			case 'D':
				output.add(true);
				output.add(true);
				output.add(false);
				output.add(true);
				break;
			case 'E':
				output.add(true);
				output.add(true);
				output.add(true);
				output.add(false);
				break;
			case 'F':
				output.add(true);
				output.add(true);
				output.add(true);
				output.add(true);
				break;
			default:
				System.out.println("Wrong file!");
				break;
		}
		return output;
	}

	public static int parseGenericPacket(List<Boolean> bits)
	{
		//If the version number is 0, we can assume the rest of the packet is 0s
		if(!bits.get(0).booleanValue() && !bits.get(1).booleanValue() && !bits.get(2).booleanValue())
		{
			return 0;
		}
		/*
		 * sublist of bits 0-2 ; convert to int; this is the version number. Add it to the total
		 * sublist of bits 3-5; convert to int; this is the type ID
		 * if typeID == 4, run number function
		 * else
		 *		if bit5, convert 6-17 to int. else, convert 6-21 to int.
		 *		sublist of bits  (18 || 22)-(converted int); pass into parseGenericPacket
		 * return total
		 */

	}

	/**
	 * Not strictly necessary for part 1.
	 */
	public static int parseNumberPacket(List<Boolean> bits)
	{
		/*
		 * iteration count = bits.length / 5
		 * output = 0
		 * for(iteration)
		 *		if(bit[i] == 1) sublist bits (i+1)-(i+4)
		 *			output = bitToInt(sublist, output)
		 * return output
		 */
	}

	public static void main(String[] args)
	{
		/*
		 * arraylist of boolean
		 * Import file
		 *
		 * for each character in the first line of the file...
		 *		convert from hex to temp boolean arraylist
		 *		arraylist.addall(temp)
		 *
		 * separate function to parse things for easy recursion...
		 *		output = parseList(arraylist)
		 *
		 * print output
		 *		
		 */
	}
}
